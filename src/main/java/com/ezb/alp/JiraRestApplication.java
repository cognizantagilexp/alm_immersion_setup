package com.ezb.alp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
 public class JiraRestApplication extends SpringBootServletInitializer {
//public class JiraRestApplication implements CommandLineRunner {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	RestTemplate restTemplate;
	String boardId = "39";
	String projectKey = "EX"; // need to read from excel

	public static String baseURL = "https://labsetuptrial.atlassian.net/rest/agile/1.0/";

	
	  @Override protected SpringApplicationBuilder
	  configure(SpringApplicationBuilder application) { return
	  application.sources(JiraRestApplication.class); }
	 

	public static void main(String[] args) {
		SpringApplication.run(JiraRestApplication.class, args);
	}

	/*@Override
	public void run(String... args) throws Exception {
		System.out.println(restTemplate);
		String ibody = "{\"event\": \"room_message\",\"item\": {\"message\": {\"date\": \"2017-09-13T09:20:31.077550+00:00\",\"from\": {\"id\": 4629418,\"links\": {\"self\": \"https://api.hipchat.com/v2/user/4629418\"},\"mention_name\": \"DevAgileLabAdmin\",\"name\": \"DevAgileLabAdmin\",\"version\": \"10BQNL7P\"},\"id\": \"6af7132d-9e37-4c21-832a-aa9f2687101f\",\"mentions\": [],\"message\": \"/createSprint SCRUMALP_R2.Sprint_2\",\"type\": \"message\"},\"room\": {\"id\": 3180489,\"is_archived\": false,\"links\": {\"participants\": \"https://api.hipchat.com/v2/room/3180489/participant\",\"self\": \"https://api.hipchat.com/v2/room/3180489\",\"webhooks\": \"https://api.hipchat.com/v2/room/3180489/webhook\"},\"name\": \"test\",\"privacy\": \"public\",\"version\": \"273BCACQ\"}},\"oauth_client_id\": \"f79d9e4f-1482-4a5b-a100-2cbf4ed19e28\",\"webhook_id\": 19185585}";
		createSprint(ibody);
	}

	

	private String credentials() {
		String plainCreds = "DevAgileLabAdmin@cognizant.com:pass1234";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		logger.info(base64Creds);
		return base64Creds;
	}*/

}
