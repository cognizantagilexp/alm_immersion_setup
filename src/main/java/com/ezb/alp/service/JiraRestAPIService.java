package com.ezb.alp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JiraRestAPIService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public void createBacklog() {
		logger.info("createbacklog service invoked");
	}

	public void createSprint() {
		logger.info("createSprint service invoked");

	}

	public void updateStories() {
		logger.info("updateStories service invoked");

	}

}
