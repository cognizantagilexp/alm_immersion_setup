package com.ezb.alp.utils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.json.JSONArray;
import org.json.JSONObject;

public class ExcelToJSON {
	private static ExcelToJSON exceltojson;

	public static ExcelToJSON getInstance() {
		if (exceltojson == null) {
			exceltojson = new ExcelToJSON();
		}
		return exceltojson;
	}

	// CSV file header
	private static final String[] FILE_HEADER_MAPPING = { "SprintName", "Issue_description", "StartDate", "EndDate", "Assigned" };

	// Student attributes
	public static final String SPRINT_NAME = "SprintName";
	public static final String ISSUE_DESC = "Issue_description";
	public static final String START_DATE = "StartDate";
	public static final String END_DATE = "EndDate"; //Assigned
	public static final String ASSIGNED = "Assigned";

	public static JSONArray readCsvFile(String fileName) {

		FileReader fileReader = null;

		CSVParser csvFileParser = null;
		JSONArray sprints = new JSONArray();

		// Create the CSVFormat object with the header mapping
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(FILE_HEADER_MAPPING);

		try {

			// Create a new list of student to be filled by CSV file data

			// initialize FileReader object
			fileReader = new FileReader(fileName);

			// initialize CSVParser object
			csvFileParser = new CSVParser(fileReader, csvFileFormat);

			// Get a list of CSV file records
			List csvRecords = csvFileParser.getRecords();

			// Read the CSV file records starting from the second record to skip
			// the header
			for (int i = 1; i < csvRecords.size(); i++) {
				CSVRecord record = (CSVRecord) csvRecords.get(i);
				// Create a new student object and fill his data
				JSONObject sprint = new JSONObject();
				sprint.put(SPRINT_NAME, record.get(SPRINT_NAME));
				sprint.put(ISSUE_DESC, record.get(ISSUE_DESC));
				sprint.put(START_DATE, record.get(START_DATE));
				sprint.put(END_DATE, record.get(END_DATE));
				sprint.put(ASSIGNED, record.get(ASSIGNED));

				// Student student = new
				// Student(Long.parseLong(record.get(STUDENT_ID)),
				// record.get(STUDENT_FNAME), record.get(STUDENT_LNAME),
				// record.get(STUDENT_GENDER),
				// Integer.parseInt(record.get(STUDENT_AGE)));
				sprints.put(sprint);
			}

			/*
			 * //Print the new student list for (Student student : sprints) {
			 * System.out.println(student.toString()); }
			 */
		} catch (Exception e) {
			System.out.println("Error in CsvFileReader !!!");
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
				csvFileParser.close();
			} catch (IOException e) {
				System.out.println("Error while closing fileReader/csvFileParser !!!");
				e.printStackTrace();
			}
		}
		return sprints;

	}

}
