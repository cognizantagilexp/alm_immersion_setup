package com.ezb.alp.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ezb.alp.utils.ExcelToJSON;

@RestController
@RequestMapping("/")
public class JiraRestAPIController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	RestTemplate restTemplate;
	@Value("${baseURL}")
	public String baseURL;
	@Value("${filepath}")
	public String filepath;
	@Value("${boardId}")
	public String boardId;
	@Value("${plainCreds}")
	String plainCreds;
	@Value("${baseURL_assignee}")
	String baseURL_assignee;
	HashMap<String, Integer> start_date = new HashMap<String, Integer>();
	HashMap<String, Integer> end_date = new HashMap<String, Integer>();

	/*
	 * @RequestMapping(value = "/testconfig", method = RequestMethod.GET,
	 * produces = "application/json") public void testconfig() {
	 * logger.info("base url is :- "+baseURL);
	 * logger.info("filepath is :- "+filepath);
	 * logger.info("boardid is :- "+boardId); logger.info("test completed"); }
	 */

	@RequestMapping(value = "/createsprint", method = RequestMethod.POST, produces = "application/json")
	public String createSprint(@RequestBody String ibody) {
		logger.info("createSprint invoked");
		logger.info(ibody);

		JSONObject o = new JSONObject();

		try {
			String url = baseURL + "sprint";
			String base64Creds = credentials();
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Basic " + base64Creds);
			headers.add("Content-Type", "application/json");
			JSONObject ibodyjson = new JSONObject(ibody);
			JSONObject reqbodyjson = new JSONObject();
			JSONObject csvrecord = new JSONObject();
			JSONObject issues = new JSONObject();
			JSONObject assignee = new JSONObject();
			List sprint_desc = new ArrayList<String>();
			Set<String> sprint_names = new HashSet<String>();
			Set<String> sprint_names_dates = new HashSet<String>();
			HashMap<String, String> issues_map = new HashMap<String, String>();
			HashMap<String, String> sprint_map = new HashMap<String, String>();
			logger.info(System.getProperty("user.dir"));
			logger.info(ibodyjson.getJSONObject("item").getJSONObject("message").get("message").toString());
			JSONObject csv_extract = new JSONObject();
			String resp_string = "";
			logger.info("filepath :" + filepath);
			JSONArray sprints = ExcelToJSON.readCsvFile(filepath);
			for (int i = 0; i < sprints.length(); i++) {
				csvrecord = sprints.getJSONObject(i);
				sprint_desc.add(csvrecord.get("Issue_description").toString().trim());
				sprint_names.add(csvrecord.getString("SprintName"));
				start_date.put(csvrecord.getString("SprintName"), csvrecord.getInt("StartDate"));
				end_date.put(csvrecord.getString("SprintName"), csvrecord.getInt("EndDate"));
				logger.info("start date:" + start_date);
				logger.info("end date:" + end_date);
			}
			if (!sprint_names.isEmpty()) {
				for (String sprint : sprint_names) {

					reqbodyjson.put("name", sprint);
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
					Calendar c = Calendar.getInstance();
					c.add(Calendar.DATE, start_date.get(sprint));
					reqbodyjson.put("startDate", dateFormat.format(c.getTime()).toString() + "+00:00");
					logger.info("Start Date: " + start_date.get(sprint) + "|" + dateFormat.format(c.getTime()));
					c.setTime(new Date());
					c.add(Calendar.DATE, end_date.get(sprint));
					reqbodyjson.put("endDate", dateFormat.format(c.getTime()).toString() + "+00:00");
					logger.info("End Date: " + end_date.get(sprint) + "|" + dateFormat.format(c.getTime()));
					reqbodyjson.put("originBoardId", boardId);
					// create sprint REST call
					HttpEntity<String> request = new HttpEntity<String>(reqbodyjson.toString(), headers);
					logger.info("Create sprint called (URL): " + url);// reqbodyjson.toString()
					logger.info("reqbodyjson.toString(): " + reqbodyjson.toString());
					resp_string = restTemplate.postForObject(url, request, String.class);
					logger.info("Create sprint Response: " + resp_string);
					JSONObject resp = new JSONObject(resp_string);
					sprint_map.put(sprint, resp.get("id").toString());
				}

			}
			logger.info(sprint_desc.toString());
			// Get list of backlogs call
			String url_backlog = baseURL + "board/" + boardId
					+ "/backlog?startAt=0&maxResults=2000&fields=issues.key,summary";
			HttpEntity<String> request_backlog = new HttpEntity<String>("", headers);
			logger.info("Get boardid called (URL): " + url_backlog);
			ResponseEntity<String> resp_backlog = restTemplate.exchange(url_backlog, HttpMethod.GET, request_backlog,
					String.class);
			JSONObject resp_boardId_json = new JSONObject(resp_backlog.getBody());
			logger.info("resp_boardId_json" + resp_boardId_json.toString());

			JSONArray issues_array = new JSONArray(resp_boardId_json.getJSONArray("issues").toString());
			logger.info(issues_array.length() + "");
			logger.info(issues_array.toString());
			for (int i = 0; i < issues_array.length(); i++) {
				JSONObject jsonObject = issues_array.getJSONObject(i);
				String issueId = jsonObject.getString("id");
				logger.info(issueId);
				JSONObject fields = jsonObject.getJSONObject("fields");
				if (fields != null) {
					logger.info(fields.toString());
					Object issueDescriptionObj = fields.get("summary");
					if (issueDescriptionObj != null) {
						String issueDescription = (issueDescriptionObj + "").trim();//.replaceAll("[^a-zA-Z]+", " ")
						if (sprint_desc.contains(issueDescription)) {
							issues_map.put(issueDescription, issueId);
						}
					}
				}

			}
			logger.info("result map" + issues_map.toString());
			for (int i = 0; i < sprints.length(); i++) {
				csvrecord = sprints.getJSONObject(i);
				logger.info(csvrecord.toString());// Issue_description
				String issue_desc = csvrecord.get("Issue_description").toString();
				JSONArray issue_array = new JSONArray("[" + issues_map.get(issue_desc) + "]");
				logger.debug("issues :" + issue_array.toString());
				issues.put("issues", issue_array);
				assignee.put("name", csvrecord.get("Assigned"));
				logger.info("Assignee is : " + assignee.toString());
				logger.info(issues.toString());
				// Assign Assignee to isssue REST call
				String url_assignee = baseURL_assignee + issues_map.get(issue_desc) + "/assignee";
				logger.info("url assignee is " + url_assignee);
				logger.info("assignee" + assignee.toString());
				HttpEntity<String> request_assignee = new HttpEntity<String>(assignee.toString(), headers);
				ResponseEntity<String> resp_assignee = restTemplate.exchange(url_assignee, HttpMethod.PUT,
						request_assignee, String.class);
				logger.info(resp_assignee.getBody());
				// Move issues REST call
				String url_issues = url + "/" + sprint_map.get(csvrecord.get("SprintName")) + "/issue";
				logger.info(url_issues);
				HttpEntity<String> request_issues = new HttpEntity<String>(issues.toString(), headers);
				JSONObject resp_issues = restTemplate.postForObject(url_issues, request_issues, JSONObject.class);
				o.put("Status", "Created Sprint");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return o.toString();

	}

	@RequestMapping(value = "/startsprint", method = RequestMethod.POST, produces = "application/json")
	public String startSprint(@RequestBody String ibody) {
		logger.info("startSprint invoked");
		logger.info(ibody);
		String sprint_id = null;
		JSONObject o = new JSONObject();
		try {
			String domainurl = baseURL + "sprint/";
			String base64Creds = credentials();
			JSONObject ibodyjson = new JSONObject(ibody);
			JSONObject reqbodyjson = new JSONObject();

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Basic " + base64Creds);
			headers.add("Content-Type", "application/json");

			HttpEntity<String> request_get = new HttpEntity<String>("", headers);
			ResponseEntity<String> sprintList = restTemplate.exchange(baseURL + "board/" + boardId + "/sprint",
					HttpMethod.GET, request_get, String.class);
			logger.info("====================RESPONSE==========");
			logger.info(sprintList.getBody());
			logger.info(ibodyjson.getJSONObject("item").getJSONObject("message").toString());
			String message = ibodyjson.getJSONObject("item").getJSONObject("message").get("message").toString();
			String[] msgarray = message.split(" ");
			logger.info(msgarray[1]);

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, start_date.get(msgarray[1]));
			reqbodyjson.put("startDate", dateFormat.format(c.getTime()).toString() + "+00:00");
			logger.info("Start Date: " + start_date.get(msgarray[1]) + "|" + dateFormat.format(c.getTime()));
			c.setTime(new Date());
			c.add(Calendar.DATE, end_date.get(msgarray[1]));
			reqbodyjson.put("endDate", dateFormat.format(c.getTime()).toString() + "+00:00");
			logger.info("End Date: " + end_date.get(msgarray[1]) + "|" + dateFormat.format(c.getTime()));
			reqbodyjson.put("state", "active");
			reqbodyjson.put("goal", "test goal");

			logger.info(reqbodyjson.toString());
			JSONObject sprintListjson = new JSONObject(sprintList.getBody().toString());
			JSONArray sprintListArray = sprintListjson.getJSONArray("values");
			for (int i = 0; i < sprintListArray.length(); i++) {
				JSONObject sprint = sprintListArray.getJSONObject(i);
				if (sprint.get("name").toString().equalsIgnoreCase(msgarray[1])) {
					sprint_id = sprint.get("id").toString();
				}
			}
			String url = domainurl + sprint_id;
			logger.info(url);
			HttpEntity<String> request = new HttpEntity<String>(reqbodyjson.toString(), headers);
			JSONObject resp = restTemplate.postForObject(url, request, JSONObject.class);
			o.put("Status", "updated task as done");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return o.toString();

	}

	@RequestMapping(value = "/completesprint", method = RequestMethod.POST, produces = "application/json")
	public String completeSprint(@RequestBody String ibody) {
		logger.info("completeSprint invoked");
		logger.info(ibody);
		String sprint_id = null;
		JSONObject o = new JSONObject();
		try {
			String domainurl = baseURL + "sprint/";
			String base64Creds = credentials();
			JSONObject ibodyjson = new JSONObject(ibody);
			JSONObject reqbodyjson = new JSONObject();

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Basic " + base64Creds);
			headers.add("Content-Type", "application/json");

			HttpEntity<String> request_get = new HttpEntity<String>("", headers);
			ResponseEntity<String> sprintList = restTemplate.exchange(baseURL + "board/" + boardId + "/sprint",
					HttpMethod.GET, request_get, String.class);
			logger.info("====================RESPONSE==========");
			logger.info(sprintList.getBody());
			logger.info(ibodyjson.getJSONObject("item").getJSONObject("message").toString());
			String message = ibodyjson.getJSONObject("item").getJSONObject("message").get("message").toString();
			String[] msgarray = message.split(" ");
			logger.info(msgarray[1]);

			reqbodyjson.put("state", "closed");
			reqbodyjson.put("goal", "test goal");

			logger.info(reqbodyjson.toString());
			JSONObject sprintListjson = new JSONObject(sprintList.getBody().toString());
			JSONArray sprintListArray = sprintListjson.getJSONArray("values");
			for (int i = 0; i < sprintListArray.length(); i++) {
				JSONObject sprint = sprintListArray.getJSONObject(i);
				if (sprint.get("name").toString().equalsIgnoreCase(msgarray[1])) {
					sprint_id = sprint.get("id").toString();
				}
			}
			String url = domainurl + sprint_id;
			logger.info(url);
			HttpEntity<String> request = new HttpEntity<String>(reqbodyjson.toString(), headers);
			JSONObject resp = restTemplate.postForObject(url, request, JSONObject.class);
			o.put("Status", "updated task as done");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return o.toString();

	}

	private String credentials() {

		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		logger.info(base64Creds);
		return base64Creds;
	}

	/*
	 * public static void main(String []args) { DateFormat dateFormat = new
	 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"); Calendar c =
	 * Calendar.getInstance(); c.add(Calendar.DATE, 0);
	 * System.out.println(dateFormat.format(c.getTime()).toString()+"+00:00");
	 * 
	 * c.setTime(new Date()); c.add(Calendar.DATE, 3);
	 * System.out.println(dateFormat.format(c.getTime())); }
	 */

}
